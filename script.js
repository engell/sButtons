$(function() {
    $('.button').on('mouseover', function() {
          description = $(this).attr('alt');
          $(this).css({background: '#DCDCDC'});
          $('.button').css({filter: 'blur(.18px)'});
          $(this).css({filter: 'blur(0px)'});
          $(this).css({'box-shadow': '0 0 10px 5px #DCDCDC'});
          $('#description').append("<span id='tmp'>"+description+"</span>");
      })
      $('.button').on('mouseleave', function() {
          $(this).css({background: 'transparent'});
          $('.button').css({filter: 'blur(0px)'});
          $(this).css({'box-shadow': '0 0 0px 0px transparent'});
          $('#tmp').remove();
      });
});
